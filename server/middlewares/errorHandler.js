import createErrorResponse from "../../src/core/application/utils/errorCreator.js";

export const notFoundError = (req, res, next) => {
  const error = new Error(`Can't find ${req.originalUrl} on the server.`);
  error.statusCode = 404;
  next(error);
};

export const globalErrorHandler = (error, req, res, next) => {
  error.statusCode = error.statusCode || 500;
  createErrorResponse(res, error.statusCode, error.message);
};
