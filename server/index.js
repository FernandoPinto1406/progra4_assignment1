import express from "express";
import bodyParser from "body-parser";
import config from "./config/configVariables.js";
import connectToMongoDB from "../src/data/mongoDbPersistance.js";
import gamesRoutes from "../src/api/routes/gamesRoutes.js";
import requestLogger from "./middlewares/requestLogger.js";
import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";
import swaggerConfig from "../src/api/swagger/swaggerConfig.js";
import {
  notFoundError,
  globalErrorHandler,
} from "./middlewares/errorHandler.js";

connectToMongoDB();
const app = express();
app.use(bodyParser.json());
app.use(express.json());
app.use(requestLogger);

app.get("/", (req, res) =>
  res.send("This is the VideoGames Store. Please add '/api-doc/' to the URL.")
);
app.use("/api/v1", gamesRoutes);

app.listen(config.PORT, () =>
  console.log(`Server Running on port: http://localhost:${config.PORT}`)
);

app.use(
  "/api-doc",
  swaggerUI.serve,
  swaggerUI.setup(swaggerJsDoc(swaggerConfig))
);

app.all("*", notFoundError);
app.use(globalErrorHandler);
