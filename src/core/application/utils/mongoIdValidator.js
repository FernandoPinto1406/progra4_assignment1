import mongoose from "mongoose";
import createErrorResponse from "./errorCreator.js";

export const validateId = (req, res, next) => {
  const { id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return createErrorResponse(res, 422, "Invalid ID provided.");
  }
  next();
};
