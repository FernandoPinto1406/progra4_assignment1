const createErrorResponse = (res, statusCode, message) => {
    return res.status(statusCode).json({
      error: {
        statusCode: statusCode,
        message: message,
      },
    });
  };
  
  export default createErrorResponse;
  