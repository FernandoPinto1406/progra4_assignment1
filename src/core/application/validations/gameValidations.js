export const validateAllFieldsAreRequired = (req, res, next) => {
  const { name, description, genre, platform } = req.body;
  if (!name || !description || !genre || !platform) {
    return res.status(422).json({
      status: 422,
      message: "All fields are required.",
    });
  }
  next();
};
