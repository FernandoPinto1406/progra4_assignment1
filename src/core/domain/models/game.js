import mongoose from "mongoose";

const gameSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    genre: {
      type: String,
      required: true,
    },
    platform: {
      type: String,
      required: true,
    },
  },
  { versionKey: false }
);

export default mongoose.model("Game", gameSchema);
