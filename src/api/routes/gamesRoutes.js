import express from "express";

import { validateId } from "../../core/application/utils/mongoIdValidator.js";
import { validateAllFieldsAreRequired } from "../../core/application/validations/gameValidations.js";

import {
  createGame,
  getGames,
  getGameById,
  updateGameById,
  patchGameById,
  deleteGameById,
} from "../controllers/gamesController.js";

const router = express.Router();
const gamesPath = "/games";

router
  .get(gamesPath, getGames)
  .post(gamesPath, validateAllFieldsAreRequired, createGame)
  .get(`${gamesPath}/:id`, validateId, getGameById)
  .put(
    `${gamesPath}/:id`,
    validateId,
    validateAllFieldsAreRequired,
    updateGameById
  )
  .patch(`${gamesPath}/:id`, validateId, patchGameById)
  .delete(`${gamesPath}/:id`, validateId, deleteGameById);

export default router;
