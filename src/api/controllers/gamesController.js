import gameSchema from "../../core/domain/models/game.js";
import createErrorResponse from "../../core/application/utils/errorCreator.js";


export const getGames = (req, res, next) => {
  gameSchema.find()
    .then((data) => {
      if (data.length === 0) {
        return createErrorResponse(res, 404, "No games found.");
      }
      res.status(200).json(data);
    })
    .catch(next);
};

export const createGame = (req, res, next) => {
  const gameToSave = new gameSchema(req.body);

  gameToSave.save()
    .then((data) => {
      const responseData = {
        _id: data.id,
        name: data.name,
        description: data.description,
        genre: data.genre,
        platform: data.platform,
      };
      res.status(201).json(responseData);
    })
    .catch((error) =>
      createErrorResponse(res, 422, error.message)
    );
};

export const getGameById = (req, res, next) => {
  const { id } = req.params;

  gameSchema.findById(id)
    .then((data) => {
      if (!data) {
        return createErrorResponse(res, 404, "Game not found.");
      }
      res.status(200).json(data);
    })
    .catch((error) => next(error));
};

export const updateGameById = (req, res, next) => {
  const { id } = req.params;
  const { name, description, genre, platform } = req.body;

  gameSchema.findByIdAndUpdate(
    id,
    { name, description, genre, platform },
    { new: true }
  )
    .then((updatedGame) => {
      if (!updatedGame) {
        return createErrorResponse(res, 404, "Game not found.");
      }
      res.status(200).json(updatedGame);
    })
    .catch((error) => next(error));
};

export const patchGameById = (req, res, next) => {
  const { id } = req.params;
  const { name, description, genre, platform } = req.body;

  const updateFields = {};
  if (name) updateFields.name = name;
  if (description) updateFields.description = description;
  if (genre) updateFields.genre = genre;
  if (platform) updateFields.platform = platform;

  gameSchema.findOneAndUpdate(
    { _id: id },
    { $set: updateFields },
    { new: true }
  )
    .then((updatedGame) => {
      if (!updatedGame) {
        return createErrorResponse(res, 404, "Game not found.");
      }
      res.status(200).json(updatedGame);
    })
    .catch((error) => next(error));
};

export const deleteGameById = (req, res, next) => {
  const { id } = req.params;

  gameSchema.findByIdAndDelete(id)
    .then((gameToDelete) => {
      if (!gameToDelete) {
        return createErrorResponse(res, 404, "Game not found.");
      }
      res.status(204).json();
    })
    .catch((error) => next(error));
};