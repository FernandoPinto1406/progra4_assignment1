/**
 * @swagger
 * components:
 *   schemas:
 *     Game:
 *       type: object
 *       required:
 *         - _id
 *         - name
 *         - description
 *         - genre
 *         - platform
 *       properties:
 *         _id:
 *           type: ObjectId
 *           description: The auto-generated _id of the game.
 *         name:
 *           type: string
 *           description: The name of the game
 *         description:
 *           type: string
 *           description: The description of the game.
 *         genre:
 *           type: string
 *           description: The genre of the game.
 *         platform:
 *           type: string
 *           description: The platform of the game.
 *       example:
 *         _id: 668432d699c2d2c00cd24646
 *         name: Minecraft
 *         description: Blocks
 *         genre: Survival
 *         platform: PC
 *     GameDTO:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - genre
 *         - platform
 *       properties:
 *         name:
 *           type: string
 *           description: The name of the game
 *         description:
 *           type: string
 *           description: The description of the game.
 *         genre:
 *           type: string
 *           description: The genre of the game.
 *         platform:
 *           type: string
 *           description: The platform of the game.
 *       example:
 *         name: Minecraft
 *         description: Blocks
 *         genre: Survival
 *         platform: PC
 */

/**
 * @swagger
 * tags:
 *   name: Games
 *   description: The games managing API.
 */

/**
 * @swagger
 * /api/v1/games:
 *   get:
 *     summary: Returns the list of all the games.
 *     tags: [Games]
 *     responses:
 *       200:
 *         description: The list of the games.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Game'
 *       404:
 *         description: No games found.
 *       500:
 *         description: Some server error.
 */

/**
 * @swagger
 * /api/v1/games/{id}:
 *   get:
 *     summary: Get the game by id.
 *     tags: [Games]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The game id.
 *     responses:
 *       200:
 *         description: The game description by id.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Game'
 *       404:
 *         description: The game was not found.
 *       422:
 *         description: Invalid Data.
 *       500:
 *         description: Some server error.
 */

/**
 * @swagger
 * /api/v1/games:
 *   post:
 *     summary: Create a new game.
 *     tags: [Games]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GameDTO'
 *     responses:
 *       200:
 *         description: The game was successfully created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Game'
 *       422:
 *         description: Invalid Data.
 *       500:
 *         description: Some server error.
 */

/**
 * @swagger
 * /api/v1/games/{id}:
 *   put:
 *     summary: Update the game by the id.
 *     tags: [Games]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The game id.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GameDTO'
 *     responses:
 *       200:
 *         description: The game was updated.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Game'
 *       404:
 *         description: The game was not found.
 *       422:
 *         description: Invalid Data.
 *       500:
 *         description: Some server error.
 */

/**
 * @swagger
 * /api/v1/games/{id}:
 *   patch:
 *     summary: Patch the game by the id.
 *     tags: [Games]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The game id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GameDTO'
 *     responses:
 *       200:
 *         description: The game was patched.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Game'
 *       404:
 *         description: The game was not found.
 *       422:
 *         description: Invalid Data.
 *       500:
 *         description: Some server error.
 */

/**
 * @swagger
 * /api/v1/games/{id}:
 *   delete:
 *     summary: Remove the game by id.
 *     tags: [Games]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The game id.
 *     responses:
 *       204:
 *         description: The game was deleted.
 *       404:
 *         description: The game was not found.
 *       422:
 *         description: Invalid Data.
 *       500:
 *         description: Some server error.
 */
