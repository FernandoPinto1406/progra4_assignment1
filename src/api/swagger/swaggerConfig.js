import { fileURLToPath } from "url";
import { dirname, join } from "path";
import config from "../../../server/config/configVariables.js";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const swaggerConfig = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Games API",
      version: "1.0.0",
    },
    servers: [
      {
        url: `http://localhost:${config.PORT}`,
      },
    ],
  },
  apis: [join(__dirname, "./swaggerDocs.js")],
};

export default swaggerConfig;
